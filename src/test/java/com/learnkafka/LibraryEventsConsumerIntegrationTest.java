package com.learnkafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learnkafka.model.Book;
import com.learnkafka.model.LibraryEvent;
import com.learnkafka.model.LibraryEventType;
import com.learnkafka.repository.LibraryEventsRepository;
import com.learnkafka.service.LibraryEventConsumer;
import com.learnkafka.service.LibraryEventService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EmbeddedKafka(topics = {"library-events"}, partitions = 3)
@TestPropertySource(properties = {"spring.kafka.producer.bootstrap-servers=${spring.embedded.kafka.brokers}",
        "spring.kafka.consumer.bootstrap-servers=${spring.embedded.kafka.brokers}"})
public class LibraryEventsConsumerIntegrationTest {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    EmbeddedKafkaBroker embeddedKafkaBroker;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    KafkaTemplate<Integer,String> kafkaTemplate;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    KafkaListenerEndpointRegistry endpointRegistry;

    @SpyBean
    LibraryEventConsumer libraryEventConsumerSpy;

    @SpyBean
    LibraryEventService libraryEventServiceSpy;

    @Autowired
    LibraryEventsRepository repository;

    @Autowired
    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    void setUp() {

        for (MessageListenerContainer messageListenerContainer : endpointRegistry.getListenerContainers()){
            ContainerTestUtils.waitForAssignment(messageListenerContainer,embeddedKafkaBroker.getPartitionsPerTopic());
        }
    }

    @AfterEach
    void tearDown() {
        repository.deleteAll();
    }

    @Test
    void publishNewLibraryEvent() throws ExecutionException, InterruptedException, JsonProcessingException {
        //given
        String json = "{\"libraryEventId\":23,\"book\":{\"id\":456,\"bookName\":\"Kafka using Spring boot\",\"bookAuthor\":\"Tofik\"},\"libraryEventType\":\"NEW\"}";
        kafkaTemplate.sendDefault(json).get();

        //when
        CountDownLatch latch = new CountDownLatch(1);
        latch.await(3, TimeUnit.SECONDS);

        //then
        verify(libraryEventConsumerSpy,times(1)).onMessage(isA(ConsumerRecord.class));
        verify(libraryEventServiceSpy,times(1)).processLibraryEVent(isA(ConsumerRecord.class));

        List<LibraryEvent> all = (List<LibraryEvent>) repository.findAll();
        assert all.size()==1;
        all.stream().forEach(libraryEvent -> {
            assert libraryEvent.getLibraryEventId()!=null;
            assertEquals(456,libraryEvent.getBook().getId());
        });
    }

    @Test
    void publishUpdateLibraryEvent() throws JsonProcessingException, ExecutionException, InterruptedException {
        //given
        String json = "{\"libraryEventId\":null,\"book\":{\"id\":456,\"bookName\":\"Kafka using Spring boot\",\"bookAuthor\":\"Tofik\"},\"libraryEventType\":\"UPDATE\"}";
        LibraryEvent libraryEvent = mapper.readValue(json,LibraryEvent.class);
        libraryEvent.getBook().setLibraryEvent(libraryEvent);
        repository.save(libraryEvent);
        //publish the update LibraryEvent
        Book updatedBook = Book.builder()
                .id(456)
                .bookAuthor("Tofik")
                .bookName("Kafka using Spring boot")
                .build();
        libraryEvent.setLibraryEventType(LibraryEventType.UPDATE);
        libraryEvent.setBook(updatedBook);
        String updatedJson = mapper.writeValueAsString(libraryEvent);
        kafkaTemplate.sendDefault(libraryEvent.getLibraryEventId(),updatedJson).get();

        //when
        CountDownLatch latch = new CountDownLatch(1);
        latch.await(3, TimeUnit.SECONDS);

        //then
//        verify(libraryEventConsumerSpy,times(1)).onMessage(isA(ConsumerRecord.class));
//        verify(libraryEventServiceSpy,times(1)).processLibraryEVent(isA(ConsumerRecord.class));
        LibraryEvent libraryEvent1 = repository.findById(libraryEvent.getLibraryEventId()).get();

        assertEquals("Kafka using Spring boot",libraryEvent1.getBook().getBookName());
    }

    @Test
    void publishUpdateLibraryEventThenException() throws JsonProcessingException, ExecutionException, InterruptedException {
        //given
        String json = "{\"libraryEventId\":5,\"book\":{\"id\":456,\"bookName\":\"Kafka using Spring boot\",\"bookAuthor\":\"Tofik\"},\"libraryEventType\":\"UPDATE\"}";
        LibraryEvent libraryEvent = mapper.readValue(json,LibraryEvent.class);
        kafkaTemplate.sendDefault(libraryEvent.getLibraryEventId(),json).get();

        //when
        CountDownLatch latch = new CountDownLatch(1);
        latch.await(3, TimeUnit.SECONDS);

        //then
        verify(libraryEventConsumerSpy,times(1)).onMessage(isA(ConsumerRecord.class));
        verify(libraryEventServiceSpy,times(1)).processLibraryEVent(isA(ConsumerRecord.class));

        Optional<LibraryEvent> libraryEvent1 = repository.findById(libraryEvent.getLibraryEventId());

        assertFalse(libraryEvent1.isPresent());
    }

    @Test
    void publishUpdateLibraryEventThenExceptionWhenNull() throws JsonProcessingException, ExecutionException, InterruptedException {
        //given
        String json = "{\"libraryEventId\":null,\"book\":{\"id\":456,\"bookName\":\"Kafka using Spring boot\",\"bookAuthor\":\"Tofik\"},\"libraryEventType\":\"UPDATE\"}";
        LibraryEvent libraryEvent = mapper.readValue(json,LibraryEvent.class);
        kafkaTemplate.sendDefault(libraryEvent.getLibraryEventId(),json).get();

        //when
        CountDownLatch latch = new CountDownLatch(1);
        latch.await(3, TimeUnit.SECONDS);

        //then
        verify(libraryEventConsumerSpy,times(1)).onMessage(isA(ConsumerRecord.class));
        verify(libraryEventServiceSpy,times(1)).processLibraryEVent(isA(ConsumerRecord.class));

    }

    @Test
    void publishUpdateLibraryEventThenExceptionWhenIdIs_000() throws JsonProcessingException, ExecutionException, InterruptedException {
        //given
        Integer libraryEventId = 000;
        String json = "{\"libraryEventId\":"+libraryEventId+",\"book\":{\"id\":456,\"bookName\":\"Kafka using Spring boot\",\"bookAuthor\":\"Tofik\"},\"libraryEventType\":\"UPDATE\"}";
        LibraryEvent libraryEvent = mapper.readValue(json,LibraryEvent.class);
        kafkaTemplate.sendDefault(libraryEvent.getLibraryEventId(),json).get();

        //when
        CountDownLatch latch = new CountDownLatch(1);
        latch.await(3, TimeUnit.SECONDS);

        //then
//        verify(libraryEventConsumerSpy,times(3)).onMessage(isA(ConsumerRecord.class));
        verify(libraryEventServiceSpy,times(4)).processLibraryEVent(isA(ConsumerRecord.class));
        verify(libraryEventServiceSpy,times(1)).handleRecovery(isA(ConsumerRecord.class));
    }
}
